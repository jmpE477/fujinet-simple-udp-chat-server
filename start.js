const _ = require('lodash');
const dgram = require('dgram');
const server = dgram.createSocket('udp4');

const serverConfig = {
    welcome: '** Welcome on fujinet.pl shoutbox       ** type /help to get more info',
    serverPort: 6502,
    sendingInterval: 500, //msec
    inactivityCheckInterval: 30, //sec
    pingAfter: 60, //sec
    deactivateAfter: 150, //sec
    debug: true
}

helpLine = [
    "** /help         - this help screen",
    "** /nick <name>  - change name",
    "** /who          - list active users",
    "** /exit         - leave"
]

const users = [];
const messages = [];
const outerQueue = [];

const scheduleQueue = () => setTimeout(processQueue, serverConfig.sendingInterval);

const debug = txt => { if (serverConfig.debug) console.log(txt) };

const processQueue = () => {
    if (outerQueue.length > 0) {
        const m = outerQueue.shift();
        //console.log(m);
        server.send(m.body, m.port, m.address, scheduleQueue);
    } else {
        scheduleQueue();
    }
}

const traceInactivity = () => {
    const now = nowStamp();
    _.each(users, user => {
        if  (user.active) {
            if (now - user.lastAction > serverConfig.pingAfter) {
                pingUser(user);
            };
            if (now - user.lastAction > serverConfig.deactivateAfter) {
                registerUserAction(user, false);
                debug(`deactivated user ${user.name}`);
                broadcastStatus();
            };
        };
    });
    setTimeout(traceInactivity, serverConfig.inactivityCheckInterval * 1000);
}

const addToQueue = (body, port, address) => outerQueue.push({body, port, address});

const getHeader = msg => {
    const msg8 = Uint8Array.from(msg)
    return {
        h1: String.fromCharCode(msg[0]),
        command: String.fromCharCode(msg[1]),
        paramL: msg8[2],
        paramH: msg8[3],
        h2: String.fromCharCode(msg[4]),
        body: msg.toString().substring(5)
    }
}

const validateHeader = header => {
    return (header.h1 == '<') && (header.h2 == '>')
}

const getUser = rinfo => _.find(users, {'address': rinfo.address, 'port': rinfo.port});

const getActive = () => _.filter(users, {'active': true});

const sendStatus = rinfo => {
    debug(`Status request from ${rinfo.address}:${rinfo.port}`);
    const user = getUser(rinfo);
    addToQueue(`<S${String.fromCharCode(user && user.active?1:0)}${String.fromCharCode(getActive().length)}>${serverConfig.welcome}`, rinfo.port, rinfo.address);
    if (user && user.active)
        addToQueue(`<A  >${user.name}`, rinfo.port, rinfo.address);
}

const pingUser = user => {
    debug(`Pinging user: ${user.name}`);
    addToQueue(`<P  >PING!`, user.port, user.address);
}

const nowStamp = () => Math.floor(Date.now() / 1000);

const registerUserAction = (user, active = true) => {
    user.lastAction = nowStamp();
    user.active = active;
}

const nameInUse = name => _.find(users, {'name': name, 'active': true});

const setName = (rinfo, name) => {
    debug(`Auth request from ${rinfo.address}:${rinfo.port}`);
    if (nameInUse(name)) {
        addToQueue(`<M${String.fromCharCode(0)} >** Name in use. Type /nick to set new one`, rinfo.port, rinfo.address);
    } else {
        addToQueue(`<M${String.fromCharCode(1)} >** Authorization OK`, rinfo.port, rinfo.address);
        addToQueue(`<A  >${name}`, rinfo.port, rinfo.address);
        const user = getUser(rinfo);
        if (user) {
            user.name = name;
            registerUserAction(user);
            debug(`Name updated`);
        } else {
            users.push({
                address: rinfo.address,
                port: rinfo.port,
                name: name,
                lastAction: nowStamp(),
                active: true
            })
            debug(`User Added`);
        }        
        broadcastStatus();
    }
}

const broadcast = msg => {
    _.each(users, user => {
        if (user.active) {
            addToQueue(`${msg}`, user.port, user.address);
        }
    });
}

const broadcastStatus = msg => {
    _.each(users, user => {
        if (user.active) {
            addToQueue(`<S ${String.fromCharCode(getActive().length)}>`, user.port, user.address);
        }
    });
}

const sendMessage = (rinfo, header) => {
    debug(`Message from ${rinfo.address}:${rinfo.port}: ${header.body}`);
    const user = getUser(rinfo);
    if (user) {
        broadcast(`<M  ><${user.name}>${header.body}`);
        registerUserAction(user);
        debug(`Message broadcasted`);
    } else {
        addToQueue(`<M  >** Unauthorized. Type /nick to set name.`, rinfo.port, rinfo.address);
    }
}

const runCommand = (rinfo, header) => {
    const user = getUser(rinfo);
    const command = _.split(header.body.substring(1), ' ');
    debug(`Command from ${rinfo.address}:${rinfo.port}: ${command[0]}`);
    switch (command[0].toUpperCase()) {
        case 'NICK':
            setName(rinfo, _.join(_.tail(command), ' '));
        break;
        case 'HELP':
            _.each(helpLine, help => {
                addToQueue(`<M  >${help}`, rinfo.port, rinfo.address);
            });
        break;
        case 'WHO':
                addToQueue(`<M  >** Users (${getActive().length}): ${ _.join(_.map(getActive(),u=>user.name),',')}`, rinfo.port, rinfo.address);
        break;
        case 'EXIT':
            server.send(`<X  >** Good Bye!`, rinfo.port, rinfo.address);
            registerUserAction(user, false);
            broadcastStatus;
        break;
        default:
            addToQueue(`<M  >** Unknown command`, rinfo.port, rinfo.address);
        break;
    }
}

const parseDatagram = (msg, rinfo) => {
    msg = msg || '';
    const header = getHeader(msg);
    if (validateHeader(header)) {
        switch (header.command) {
            case 'S':  // status
                sendStatus(rinfo);
            break;
            case 'A':  // auth
                setName(rinfo, header.body);
            break;
            case 'M':  // message
                sendMessage(rinfo, header);
            break;
            case 'C':  // command
                runCommand(rinfo, header);
            break;
            case 'P':  // ping
                const user = getUser(rinfo);
                if (user) registerUserAction(user);
            break;
        }
    } else {
        debug(`Invalid header: ${msg} from ${rinfo.address}:${rinfo.port}`);
    }
}

server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});

server.on('message', (msg, rinfo) => {
    debug(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
    parseDatagram(msg, rinfo)
});

server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
});

server.bind(serverConfig.serverPort);
scheduleQueue();
traceInactivity();